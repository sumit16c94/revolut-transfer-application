A)Run Application :

1) you can directly run the application as an executable jar using the command 
	 java -jar target/RestApplication-1.0-SNAPSHOT.jar server config.yml

2) The application starts on port 9000 with integrated swagger :
   url : http://localhost:9000/swagger/
3) After reaching the Url click on 'List Operations' which exsists on the top right of the UI.

B)To build the application clone the application and run :
   mvn clean install.
3) The application is built using the Dropwizard framework .
4) The in-memory database used is HSQLDB which consists of two main entities :
    customer - primary_key -> cust_id
    account_details primary_key -> iban linked to customer where account_details.fk_cust_id = customer.cust_id
5) The datasource factory is configured to achieve the connenction pooling .
6) Unit test in Junit and mockito
7) Ignored the topic of currency_conversion for simplicity.
