package com.test.dropwizard.serviceImpl;

import com.test.dropwizard.Exceptions.AccountDetailsException;
import com.test.dropwizard.Exceptions.LockException;
import com.test.dropwizard.dto.AccountTransferDTO;
import com.test.dropwizard.model.AccountDetails;
import com.test.dropwizard.service.AccountService;

import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.statement.Update;
import org.jdbi.v3.core.transaction.TransactionIsolationLevel;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 *  * Allows businesss logic  for {@link AccountDetails }domain-model based on the specified criterion
 *
 *  */

public class AccountServiceImpl implements AccountService {

    private final Jdbi jdbi;


    public AccountServiceImpl(Jdbi jdbi) {
        this.jdbi = jdbi;

    }

    public boolean computeTransfer(
            final AccountDetails from, final AccountDetails to, final long amount)
            throws LockException, InterruptedException {
        final AccountDetails[] accountDetails = new AccountDetails[]{from, to};
        Arrays.sort(accountDetails);
        if (accountDetails[0].monitor.tryLock(1, TimeUnit.SECONDS)) {
            try {
                if (accountDetails[1].monitor.tryLock(1, TimeUnit.SECONDS)) {
                    try {
                        if (from.withdraw(amount)) {
                            to.deposit(amount);
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    finally {
                        accountDetails[1].monitor.unlock();
                    }
                }
            }
            finally {
                accountDetails[0].monitor.unlock();
            }
        }
        throw new LockException("Unable to acquire locks on the accounts");
    }

    @Override
    public String validateTransfer(final AccountTransferDTO accountTransferDTO) {

        try {
            //check for accounts if present
            final AccountDetails from = this.getAccountByIBAN(accountTransferDTO.getFromAccountIban())
                                            .orElseThrow(() -> new AccountDetailsException(
                                                    "Receiver account with the iban No --> " + accountTransferDTO.getFromAccountIban() + " dosen't exist"));
            final AccountDetails to = this.getAccountByIBAN(accountTransferDTO.getToAccountIban())
                                          .orElseThrow(() -> new AccountDetailsException(
                                                  "Sender account with the iban No --> " + accountTransferDTO.getFromAccountIban() + " dosen't exist"));
            //Check if the transfer amount = 0
            if (accountTransferDTO.getTransferAmount() == 0) {
                return "The transfer amount should be greater than Zero";
            }


            //condition for successful transfer
            else if (this.computeTransfer(from,
                                          to,
                                          accountTransferDTO.getTransferAmount())) {
                this.saveBalance(from);
                this.saveBalance(to);
                return "Fund transfer successful \nAvailable Balance :: " +
                        this.getAccountByIBAN(accountTransferDTO.getFromAccountIban()).get().getBalance();
            }

            //validation for insufficient balance
            else {
                return "Insufficient Balance \nAvailable Balance is  " + this.getAccountByIBAN(
                        accountTransferDTO.getFromAccountIban()).get().getBalance() + " only";
            }

        }
        catch (Exception e) {
            return e.getMessage();
        }
    }

    @Transaction(TransactionIsolationLevel.SERIALIZABLE)
    public String deposit(final String toIban, final int amount) {
        String message = "";
        try {
            if (amount == 0) {
                message = "The amount to be deposited should be greater than 0";
            }
            else {
                final AccountDetails to = this.getAccountByIBAN(toIban)
                                              .orElseThrow(() -> new AccountDetailsException(
                                                      " Account with the iban No --> " + toIban + " dosen't exist"));
                to.deposit(amount);
                long updatedBalance = this.saveBalance(to) ? to.getBalance() + amount : amount;
                message = amount == updatedBalance ? " TRANSACTION UNSUCCESSFUL " : "TRANSACTION SUCCESSFUL ->\nAvailable Balance::  " + updatedBalance + "";
            }
        }
        catch (Exception e) {
            message = e.getMessage();
        }
        finally {
            return message;
        }

    }

    @Transaction(TransactionIsolationLevel.SERIALIZABLE)
    public String withdraw(final String ibanFrom, final int amount) {
        String message = "";
        try {
            if (amount == 0) {
                message = "The amount to be withdrawn should be greater than 0";
            }
            else {
                final AccountDetails from = this.getAccountByIBAN(ibanFrom)
                                                .orElseThrow(() -> new AccountDetailsException(
                                                        "Account with the iban No --> " + ibanFrom + " dosen't exist"));
                from.withdraw(amount);
                long updatedBalance = this.saveBalance(from) ? from.getBalance() - amount : amount;
                message = amount == updatedBalance ? " TRANSACTION UNSUCCESSFUL " : "TRANSACTION SUCCESSFUL ->\nAvailable Balance::  " + updatedBalance + "";

            }
        }
        catch (Exception e) {
            message = e.getMessage();
        }
        finally {
            return message;
        }
    }

    public boolean saveBalance(final AccountDetails accountDetails) {
        int execute = 0;
        try (Handle handle = jdbi.open()) {
            final Update update = handle.createUpdate(Queries.UPDATE_BALANCE);
            Map<String, Object> params = new HashMap<>();
            params.put("amount", accountDetails.getBalance());
            params.put("iban", accountDetails.getIban());
            update.bindMap(params);
            execute = update.execute();

        }
        catch (Exception e) {
            Logger.getLogger("Exception occured in " + AccountServiceImpl.class + " --> saveBlance()" + e);
        }
        return execute > 0;
    }


    @Override
    public int createTableAccountDetails() {
        return this.jdbi.withHandle(handle -> handle.execute(Queries.CREATE_TABLE_ACCOUNT_DETAILS));
    }

    @Override
    public List<AccountDetails> fetchAllAccounts() {
        return this.jdbi.withHandle(handle -> handle.select(Queries.FETCH_ALL_ACCOUNTS)
                                                    .mapTo(AccountDetails.class)
                                                    .collect(Collectors.toCollection(ArrayList::new)));

    }

    @Override
    public Optional<AccountDetails> getAccountByIBAN(final String iban) {
        return this.jdbi.withHandle(handle -> handle.select(Queries.GET_ACCOUNT_DETAILS)
                                                    .bind("iban", iban)
                                                    .mapTo(AccountDetails.class).findFirst());

    }


    @Override
    public String saveAccountDetails(final AccountDetails accountDetails) {
        // autoGenerated UUID as iban
        accountDetails.setIban();
        this.jdbi.withHandle(handle -> handle.createUpdate(Queries.SAVE_ACCOUNT_DETAILS)
                                             .bind("iban", String.valueOf(accountDetails.getIban()))
                                             .bind("fk_cust_id", accountDetails.getCustomer().getCustId())
                                             .bind("account_type", accountDetails.getCardType())
                                             .bind("amount", accountDetails.getBalance())
                                             .execute());
        return accountDetails.getIban();
    }

    /**
     * Interface purposefully specified with private modifier as it has no use outside this class
     * supports querying on {@link AccountDetails object to pull data from database}
     */
    private interface Queries {
        String CREATE_TABLE_ACCOUNT_DETAILS = "create table IF NOT EXISTS account_details"
                + "(iban varchar(50), fk_cust_id BIGINT, account_type varchar(100), amount BIGINT, PRIMARY KEY (iban),FOREIGN KEY (fk_cust_id) REFERENCES customer(cust_id) )";
        String SAVE_ACCOUNT_DETAILS         = "Insert into account_details (iban,fk_cust_id,account_type,amount) values (" + ":iban , :fk_cust_id, :account_type, :amount" + ")";
        String GET_ACCOUNT_DETAILS          = "select * from account_details ad join customer cust on cust.cust_id = ad.fk_cust_id  where iban = :iban";

        String UPDATE_BALANCE = "update account_details set amount=:amount where iban = :iban";

        String FETCH_ALL_ACCOUNTS = "select * from account_details ad join customer cust on cust.cust_id = ad.fk_cust_id ";

    }

}
