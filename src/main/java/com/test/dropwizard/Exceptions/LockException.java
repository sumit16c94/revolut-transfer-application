package com.test.dropwizard.Exceptions;

public class LockException extends Exception{
    public LockException(final String message){
        super(message);
    }
}
