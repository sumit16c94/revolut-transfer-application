package com.test.dropwizard.Exceptions;

public class CustomerException extends Exception {
    public CustomerException(final String message) {
        super(message);
    }
}
