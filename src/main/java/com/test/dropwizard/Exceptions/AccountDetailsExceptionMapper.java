package com.test.dropwizard.Exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import io.dropwizard.jersey.errors.ErrorMessage;

public class AccountDetailsExceptionMapper implements ExceptionMapper<AccountDetailsException> {

    @Override
    public Response toResponse(final AccountDetailsException exception) {
        return Response.status(Response.Status.NOT_FOUND)
                       .entity(new ErrorMessage(Response.Status.NOT_FOUND.getStatusCode(), exception.getMessage()))
                       .build();
    }
}
