package com.test.dropwizard.Exceptions;

public class AccountDetailsException extends RuntimeException {
    public AccountDetailsException(final String message) {
        super(message);

    }

}
