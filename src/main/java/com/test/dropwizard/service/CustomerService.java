package com.test.dropwizard.service;

import com.test.dropwizard.model.Customer;

import java.util.ArrayList;

/**
 * Service to support querying on {@link Customer domain}
 */
public interface CustomerService {

    ArrayList<Customer> fetchAllCustomers();

    Long savePerson(final Customer customer);

    int createTableCustomer();
}
