package com.test.dropwizard.service;


import com.test.dropwizard.Exceptions.AccountDetailsException;
import com.test.dropwizard.Exceptions.LockException;
import com.test.dropwizard.dto.AccountTransferDTO;
import com.test.dropwizard.model.AccountDetails;

import java.util.List;
import java.util.Optional;

/**
 * Service to support querying on {@link AccountService domain}
 */
public interface AccountService {
    boolean computeTransfer(final AccountDetails from,
                            final AccountDetails to,
                            final long amount) throws LockException, InterruptedException;

    String validateTransfer(final AccountTransferDTO accountTransferDTO);

    boolean saveBalance(final AccountDetails accountDetails);

    Optional<AccountDetails> getAccountByIBAN(String Ibn) throws AccountDetailsException;

    String saveAccountDetails(final AccountDetails accountDetails);

    int createTableAccountDetails();

    List<AccountDetails> fetchAllAccounts();

    String deposit(final String ibnOfTo, final int amount);

    String withdraw(final String ibnOfFrom, final int amount);

}
