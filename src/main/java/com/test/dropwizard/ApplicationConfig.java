package com.test.dropwizard;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import com.test.dropwizard.facade.AccountFacade;
import com.test.dropwizard.mapper.AcccountDetailsMapper;
import com.test.dropwizard.mapper.CustomerMapper;
import com.test.dropwizard.model.AccountDetails;
import com.test.dropwizard.model.Customer;
import com.test.dropwizard.resource.AccountResource;
import com.test.dropwizard.resource.CustomerResource;
import com.test.dropwizard.service.AccountService;
import com.test.dropwizard.service.CustomerService;
import com.test.dropwizard.serviceImpl.AccountServiceImpl;
import com.test.dropwizard.serviceImpl.CustomerServiceImpl;

import org.jdbi.v3.core.Jdbi;

import javax.validation.Valid;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

public class ApplicationConfig extends Configuration {
    @JsonProperty("swagger")
    public  SwaggerBundleConfiguration swaggerBundleConfiguration;
    @Valid
    @NotNull
    private DataSourceFactory          database = new DataSourceFactory();

    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory() {
        return database;
    }

    @JsonProperty("database")
    public void setDataSourceFactory(DataSourceFactory factory) {
        this.database = factory;
    }

    public void registerServices(Jdbi jdbi, Environment env) {

        //create mappers to populate data after query execution
        jdbi.registerRowMapper(new CustomerMapper());
        jdbi.registerRowMapper(new AcccountDetailsMapper());

        final CustomerService customerService = new CustomerServiceImpl(jdbi);
        final AccountService accountService = new AccountServiceImpl(jdbi);
        final AccountFacade accountFacade = new AccountFacade(accountService, customerService);

        // create Tables
        accountFacade.createTableCustomer();
        accountFacade.createTableAccountDetails();

        //insert details in table customer and account
        populateDate(accountFacade);

        //register the resource classes
        final CustomerResource customerResource = new CustomerResource(accountFacade);
        final AccountResource accountResource = new AccountResource(accountFacade);
        env.jersey().register(customerResource);
        env.jersey().register(accountResource);


    }

    public void populateDate(AccountFacade accountFacade) {
        //creating customer object
        Customer cust1 = Customer.create()
                                 .withFirstName("Jake")
                                 .withLastName("Throne")
                                 .withEmail("jake@revolut.com");

        Customer cust2 = Customer.create()
                                 .withFirstName("sherlock")
                                 .withLastName("holmes")
                                 .withEmail("sherlock@holmes.com");

        //creating acountDetails Object
        AccountDetails accountDetails_1 = AccountDetails.create()
                                                        .withBalance(1000)
                                                        .withCardType("metal")
                                                        .withCustomer(cust1);
        AccountDetails accountDetails_2 = AccountDetails.create()
                                                        .withBalance(9000)
                                                        .withCardType("plastic")
                                                        .withCustomer(cust2);
        accountFacade.saveAccountDetails(accountDetails_1);
        accountFacade.saveAccountDetails(accountDetails_2);

    }
}
