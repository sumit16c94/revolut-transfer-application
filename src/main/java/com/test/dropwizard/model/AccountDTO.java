package com.test.dropwizard.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountDTO {

    private String fromAccount;
    private String toAccount;
    private long   amount;

    public AccountDTO() {
    }

    public AccountDTO(String fromAccount, String toAccount, long amount) {
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
        this.amount = amount;
    }

    @JsonProperty
    public String getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(String fromAccount) {
        this.fromAccount = fromAccount;
    }

    @JsonProperty
    public String getToAccount() {
        return toAccount;
    }

    public void setToAccount(String toAccount) {
        this.toAccount = toAccount;
    }

    @JsonProperty
    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }


}
