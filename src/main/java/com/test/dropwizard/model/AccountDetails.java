package com.test.dropwizard.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AccountDetails implements Comparable<AccountDetails> {

    @JsonIgnore
    public final Lock monitor = new ReentrantLock();

    private  String   iban;
    @NotNull
    private long     balance;
    @NotNull
    private String   cardType;
    private Customer customer;

    //made private to avoid breaking builder pattern
    private AccountDetails() {

    }

    public static AccountDetails create() {
        return new AccountDetails();
    }

    public AccountDetails withIban(String iban) {
        this.iban = iban;
        return this;
    }

    public AccountDetails withBalance(long balance) {
        this.balance = balance;
        return this;
    }

    public AccountDetails withCustomer(Customer customer) {
        this.customer = customer;
        return this;

    }

    public AccountDetails withCardType(String cardType) {
        this.cardType = cardType;
        return this;
    }

    public int compareTo(final AccountDetails other) {
        return Integer.compare(hashCode(), other.hashCode());
    }

    public void deposit(final long amount) {
        monitor.lock();
        try {
            if (amount > 0) { balance += amount; }
        }
        finally { //In case there was an Exception we're covered
            monitor.unlock();
        }
    }

    public boolean withdraw(final long amount) {
        try {
            monitor.lock();
            if (amount > 0 && balance >= amount) {
                balance -= amount;
                return true;
            }
            return false;
        }
        finally {
            monitor.unlock();
        }
    }

    public long getBalance() {
        return balance;
    }

    public String getIban() {
        return iban;
    }

    public String getCardType() {
        return cardType;
    }


    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setIban() {
        UUID uuid = UUID.randomUUID();
        this.iban = uuid.toString();
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("monitor", monitor)
                          .add("iban", iban)
                          .add("balance", balance)
                          .add("cardType", cardType)
                          .add("customer", customer)
                          .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        AccountDetails that = (AccountDetails) o;
        return balance == that.balance &&
                Objects.equal(monitor, that.monitor) &&
                Objects.equal(iban, that.iban) &&
                Objects.equal(cardType, that.cardType) &&
                Objects.equal(customer, that.customer);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(monitor, iban, balance, cardType, customer);
    }
}
