package com.test.dropwizard.model;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;

public class Customer {


    private Long   custId;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    private String email;

    //made private to avoid breaking builder pattern
    private Customer() {
        // Needed by Jackson deserialization
    }

    public static Customer create() {
        return new Customer();
    }

    public Customer withId(Long custId) {
        this.custId = custId;
        return this;
    }

    public Customer withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public Customer withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Customer withEmail(String email) {
        this.email = email;
        return this;
    }


    @JsonProperty
    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    @JsonProperty
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty
    public String getLastName() {
        return lastName;
    }

    @JsonProperty
    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("custId", custId)
                          .add("firstName", firstName)
                          .add("lastName", lastName)
                          .add("email", email)
                          .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Customer customer = (Customer) o;
        return Objects.equal(custId, customer.custId) &&
                Objects.equal(firstName, customer.firstName) &&
                Objects.equal(lastName, customer.lastName) &&
                Objects.equal(email, customer.email);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(custId, firstName, lastName, email);
    }
}
