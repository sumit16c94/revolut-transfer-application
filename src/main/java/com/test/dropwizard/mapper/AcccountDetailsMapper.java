package com.test.dropwizard.mapper;


import com.test.dropwizard.model.AccountDetails;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AcccountDetailsMapper implements RowMapper<AccountDetails> {

    CustomerMapper customerMapper = new CustomerMapper();

    @Override
    public AccountDetails map(ResultSet rs, StatementContext ctx) throws SQLException {
        return AccountDetails.create()
                             .withIban(rs.getString("iban"))
                             .withBalance(rs.getLong("amount"))
                             .withCustomer(customerMapper.map(rs, ctx))
                             .withCardType(rs.getString("account_type"));
    }


}
