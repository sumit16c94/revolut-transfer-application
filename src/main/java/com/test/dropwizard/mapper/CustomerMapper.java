package com.test.dropwizard.mapper;

import com.test.dropwizard.model.Customer;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CustomerMapper implements RowMapper<Customer> {


    @Override
    public Customer map(ResultSet rs, StatementContext ctx) throws SQLException {
        return Customer.create()
                       .withId(rs.getLong("cust_id"))
                       .withFirstName(rs.getString("first_name"))
                       .withLastName(rs.getString("last_name"))
                       .withLastName(rs.getString("email"));
    }
}
