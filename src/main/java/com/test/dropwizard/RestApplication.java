package com.test.dropwizard;

import org.jdbi.v3.core.Jdbi;

import io.dropwizard.Application;
import io.dropwizard.configuration.ResourceConfigurationSourceProvider;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;

public class RestApplication extends Application<ApplicationConfig> {

    public static void main(String[] args) throws Exception {
        new RestApplication().run(args);
    }


    @Override
    public void initialize(Bootstrap<ApplicationConfig> serviceConfigurationBootstrap) {
        //configure swagger
        serviceConfigurationBootstrap.setConfigurationSourceProvider(new ResourceConfigurationSourceProvider());
        serviceConfigurationBootstrap.addBundle(new SwaggerBundle<ApplicationConfig>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(ApplicationConfig configuration) {
                return configuration.swaggerBundleConfiguration;
            }
        });


    }

    @Override
    public void run(ApplicationConfig configuration, Environment env) throws Exception {
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(env, configuration.getDataSourceFactory(), "hsql");

        // configure  and register all the necessary services
        configuration.registerServices(jdbi, env);

    }


}
