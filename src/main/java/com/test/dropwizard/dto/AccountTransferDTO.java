package com.test.dropwizard.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountTransferDTO {

    private String fromAccountIban;
    private String toAccountIban;
    private long   transferAmount;

    public AccountTransferDTO() {
    }

    @JsonProperty
    public String getFromAccountIban() {
        return fromAccountIban;
    }

    @JsonProperty
    public String getToAccountIban() {
        return toAccountIban;
    }

    @JsonProperty
    public long getTransferAmount() {
        return transferAmount;
    }


}
