package com.test.dropwizard.resource;


import com.codahale.metrics.annotation.Timed;
import com.test.dropwizard.dto.AccountTransferDTO;
import com.test.dropwizard.facade.AccountFacade;
import com.test.dropwizard.model.AccountDetails;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * Resource class to map all the requests with regards to the {@link AccountDetails class}
 * see {@link AccountFacade for the implementation details}
 */

@Path("/account")
public class AccountResource {
    AccountFacade accountFacade;

    public AccountResource(AccountFacade accountFacade) {
        this.accountFacade = accountFacade;
    }

    @POST
    @Timed
    @Path("/save")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes({MediaType.APPLICATION_JSON})
    public String addPerson(AccountDetails accountDetails) {
        return accountFacade.saveAccountDetails(accountDetails);
    }

    @POST
    @Path("/transfer")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes({MediaType.APPLICATION_JSON})
    public String initiateTransfer(AccountTransferDTO accountTransferDTO) {
        return accountFacade.transfer(accountTransferDTO);
    }

    @GET
    @Timed
    @Path("/get/{iban}")
    @Produces(MediaType.APPLICATION_JSON)
    public AccountDetails getAccountDetails(@PathParam("iban") String iban) {
        return accountFacade.fetchAccountDetails(iban);

    }

    @GET
    @Timed
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<AccountDetails> getPersons() {
        return accountFacade.fetchAllAccounts();
    }

    @GET
    @Timed
    @Path("/deposit")
    @Produces(MediaType.TEXT_PLAIN)
    public String depositMoney(@QueryParam("ibanTo") String ibanTo, @QueryParam("amount") int amount) {
        return accountFacade.depositMoney(ibanTo, amount);
    }


    @GET
    @Timed
    @Path("/withdraw")
    @Produces(MediaType.TEXT_PLAIN)
    public String withdrawMoney(@QueryParam("ibanFrom") String ibanFrom, @QueryParam("amount") int amount) {
        return accountFacade.withdrawMoney(ibanFrom, amount);
    }
}
