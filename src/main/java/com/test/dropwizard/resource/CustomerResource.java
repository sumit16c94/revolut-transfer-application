package com.test.dropwizard.resource;

import com.codahale.metrics.annotation.Timed;
import com.test.dropwizard.facade.AccountFacade;
import com.test.dropwizard.model.Customer;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


/**
 * Resource class to map all the requests with regards to the {@link Customer class}
 * see {@link AccountFacade for the implementation details}
 */
@Path("/customer")
public class CustomerResource {

    AccountFacade accountFacade;

    public CustomerResource(AccountFacade accountFacade) {
        this.accountFacade = accountFacade;
    }


    @GET
    @Timed
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Customer> getPersons() {
        return accountFacade.fetchAllCustomers();

    }

    @POST
    @Timed
    @Path("/save")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes({MediaType.APPLICATION_JSON})
    public String addPerson(Customer customer) {
        Long cust_id = accountFacade.saveCustomer(customer);
        return "customer save with cust_id -> " + cust_id;
    }
}
