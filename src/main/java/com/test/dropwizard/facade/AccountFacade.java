package com.test.dropwizard.facade;

import com.test.dropwizard.Exceptions.AccountDetailsException;
import com.test.dropwizard.dto.AccountTransferDTO;
import com.test.dropwizard.model.AccountDetails;
import com.test.dropwizard.model.Customer;
import com.test.dropwizard.service.AccountService;
import com.test.dropwizard.service.CustomerService;

import org.jdbi.v3.core.transaction.TransactionIsolationLevel;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.ArrayList;
import java.util.List;

/**
 * AccountFacade- A single facade
 * to redirect request to {@link AccountService,CustomerService services }
 */
public class AccountFacade {

    private final AccountService  accountService;
    private final CustomerService customerService;

    public AccountFacade(final AccountService accountService, final CustomerService customerService) {
        this.accountService = accountService;
        this.customerService = customerService;
    }

    // Transactions Isolation levels is the key .
    // serilaizable will handle  dirty read , non-repeatable read, phantom reads
    @Transaction(TransactionIsolationLevel.SERIALIZABLE)
    public String transfer(final AccountTransferDTO accountTransferDTO) {
        return accountService.validateTransfer(accountTransferDTO);
    }


    public String saveAccountDetails(final AccountDetails accountDetails) {
        Long custId = accountDetails.getCustomer().getCustId();

        //if customer doesn't exist register a new customer in the customer table
        if (custId == null || custId == 0) {
            // get new cust_id as its foreign_key in account_details table
            custId = customerService.savePerson(accountDetails.getCustomer());
        }
        accountDetails.getCustomer().setCustId(custId);
        accountDetails.setCustomer(accountDetails.getCustomer());
        return accountService.saveAccountDetails(accountDetails);
    }


    public AccountDetails fetchAccountDetails(final String iban) {
        return accountService.getAccountByIBAN(iban)
                             .orElseThrow(() -> new AccountDetailsException("Account with iban " + iban + " not found"));

    }

    public int createTableAccountDetails() {
        return accountService.createTableAccountDetails();
    }

    public List<AccountDetails> fetchAllAccounts() {
        return accountService.fetchAllAccounts();
    }


    public String depositMoney(final String selfIban, final int amount) {
        return accountService.deposit(selfIban, amount);
    }

    public String withdrawMoney(final String selfIban, final int amount) {
        return accountService.withdraw(selfIban, amount);
    }

    public int createTableCustomer() {
        return customerService.createTableCustomer();
    }

    public Long saveCustomer(final Customer customer) {
        return customerService.savePerson(customer);

    }

    public ArrayList<Customer> fetchAllCustomers() {
        return customerService.fetchAllCustomers();
    }
}
