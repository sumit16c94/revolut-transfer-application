package com.test.dropwizard.serviceImpl;

import com.test.dropwizard.model.AccountDetails;
import com.test.dropwizard.model.Customer;
import com.test.dropwizard.service.AccountService;

import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.statement.Update;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

    @Mock
    private Jdbi jdbi;

    @Mock
    private Handle handle;

    @Mock
    private Update update;

    private AccountService accountService;

    private AccountDetails accountDetails;

    @BeforeEach
    void setUp() {
        AccountDetails.create()
                      .withIban("abc-abc")
                      .withBalance(2000)
                      .withCardType("metal")
                      .withCustomer(Customer.create().withId(2l));
    }

    @Test
    void testSaveBalanceOnValidInput() {
        Mockito.when(update.execute()).thenReturn(1);
        Mockito.when(handle.createUpdate(Mockito.anyString())).thenReturn(update);
        Mockito.when(jdbi.open()).thenReturn(handle);
        accountService = new AccountServiceImpl(jdbi);
        accountDetails = AccountDetails.create()
                                       .withIban("abc-abc")
                                       .withBalance(2000)
                                       .withCardType("metal")
                                       .withCustomer(Customer.create().withId(2l));
        assertTrue(accountService.saveBalance(accountDetails));
    }

    @Test
    void testSaveBalanceOnInValidInput() {
        Mockito.when(update.execute()).thenReturn(0);
        Mockito.when(handle.createUpdate(Mockito.anyString())).thenReturn(update);
        Mockito.when(jdbi.open()).thenReturn(handle);
        accountService = new AccountServiceImpl(jdbi);
        accountDetails = AccountDetails.create()
                                       .withIban("abc-abc")
                                       .withBalance(2000)
                                       .withCardType("metal")
                                       .withCustomer(Customer.create().withId(2l));
        assertFalse(accountService.saveBalance(accountDetails));
    }

    @Test
    void testGetAccountByIBAN() {
        Mockito.when(update.execute()).thenReturn(1);

    }
}