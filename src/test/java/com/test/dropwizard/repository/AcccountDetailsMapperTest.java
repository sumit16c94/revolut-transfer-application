package com.test.dropwizard.repository;

import com.test.dropwizard.mapper.AcccountDetailsMapper;
import com.test.dropwizard.mapper.CustomerMapper;
import com.test.dropwizard.model.AccountDetails;
import com.test.dropwizard.model.Customer;

import junit.framework.Assert;

import org.jdbi.v3.core.statement.StatementContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.ResultSet;
import java.sql.SQLException;

@ExtendWith(MockitoExtension.class)
class AcccountDetailsMapperTest {

    private AcccountDetailsMapper acccountDetailsMapper;

    private CustomerMapper customerMapper;

    @Mock
    private ResultSet resultSet;

    @Mock
    private StatementContext statementContext;

    @BeforeEach
    void setUp() {

        acccountDetailsMapper = new AcccountDetailsMapper();
        customerMapper = new CustomerMapper();


    }

    @Test
    void map() throws SQLException {

        Mockito.when(resultSet.getLong("cust_id")).thenReturn(5000l);
        Mockito.when(resultSet.getString("first_name")).thenReturn("first_name");
        Mockito.when(resultSet.getString("last_name")).thenReturn("last_name");
        Mockito.when(resultSet.getString("email")).thenReturn("email");

        Mockito.when(resultSet.getLong("amount")).thenReturn(5000l);
        Mockito.when(resultSet.getString("iban")).thenReturn("ibanId");
        Mockito.when(resultSet.getString("account_type")).thenReturn("metal");

        final Customer customer = customerMapper.map(resultSet, statementContext);
        final AccountDetails accountDetails = acccountDetailsMapper.map(resultSet, statementContext);

        Assert.assertEquals(5000, accountDetails.getBalance());
        Assert.assertEquals("ibanId", accountDetails.getIban());
        Assert.assertEquals(customer, accountDetails.getCustomer());
        Assert.assertEquals("metal", accountDetails.getCardType());

    }
}
